import React, { Component } from 'react'

import InputView from './views/InputView'
import BoggleView from './views/BoggleView'
import ScoreView from './views/ScoreView'

class App extends Component {
  render() {
    return (
      <div>
        <InputView />
        <BoggleView />
        <ScoreView />
      </div>
    )
  }
}

export default App
