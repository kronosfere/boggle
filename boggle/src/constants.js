// Endpoint
export const URL = 'http://104.248.152.12:5000'

// Views
export const VIEW_INPUT = 'VIEW_INPUT'
export const VIEW_BOGGLE = 'VIEW_BOGGLE'
export const VIEW_SCORE = 'VIEW_SCORE'

// Input types
export const INPUT_PAST = 'INPUT_PAST'
export const INPUT_STRING = 'INPUT_STRING'

export const SCORE_TABLE = { 3: 1, 4: 1, 5: 2, 6: 3, 7: 5 }
export const SCORE_DEFAULT = 11
