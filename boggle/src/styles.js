const styles = theme => ({
  primaryButton: {
    marginTop: theme.spacing.unit * 3,
  },
  flexDiv: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
  },
  autoFlex: {
    flex: '1 1 auto',
    width: '100%',
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      width: '40%',
    },
  },
  textFlex: {
    width: '100%',
    marginLeft: 8,
    marginRight: 8,
    display: 'flex',
    flexWrap: 'wrap',
  },
})

export default styles
