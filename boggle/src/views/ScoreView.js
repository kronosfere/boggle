import React from 'react'
import PropTypes from 'prop-types'
import SplitText from 'react-pose-text'
import { connect } from 'react-redux'

import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'

import { Canvas } from '../component'

import styles from '../styles'
import * as action from '../redux/action'
import * as constants from '../constants'

const charPoses = {
  exit: { opacity: 0, y: 20 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30 + 300,
  },
}

class ScoreView extends React.Component {
  static displayName = 'views.ScoreView'

  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  render() {
    const { classes, dispatch, page, score, found } = this.props

    return (
      <Canvas visible={page === constants.VIEW_SCORE}>
        <Typography variant="headline">Score: {score}</Typography>
        <div className={classes.textFlex}>
          <SplitText
            key={`typo`}
            initialPose="exit"
            pose="enter"
            charPoses={charPoses}
          >
            {[...found].join(' ')}
          </SplitText>
        </div>
        <Button
          fullWidth
          variant="raised"
          color="primary"
          className={classes.primaryButton}
          onClick={() => dispatch(action.pageChange(constants.VIEW_BOGGLE))}
        >
          Try Again!
        </Button>
        <Button
          fullWidth
          variant="raised"
          color="primary"
          className={classes.primaryButton}
          onClick={() => dispatch(action.pageChange(constants.VIEW_INPUT))}
        >
          Start New Game!
        </Button>
      </Canvas>
    )
  }
}

ScoreView = withStyles(styles)(
  connect(state => ({
    page: state.page,
    score: state.score,
    found: state.found,
  }))(ScoreView)
)
export default ScoreView
