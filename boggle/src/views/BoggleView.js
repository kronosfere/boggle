import React from 'react'
import PropTypes from 'prop-types'
import SplitText from 'react-pose-text'
import { connect } from 'react-redux'

import get from 'lodash/get'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'

import { BoggleBoard, Canvas, CountdownTimer } from '../component'

import styles from '../styles'
import * as action from '../redux/action'
import * as constants from '../constants'

const charPoses = {
  exit: { opacity: 0, y: 20 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

class BoggleView extends React.Component {
  static displayName = 'views.BoggleView'

  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  initialState = props => ({
    error: false,
    found: new Set(),
    inputString: '',
    score: 0,
    timerKey: 'init',
  })

  constructor(props) {
    super(props)
    this.state = this.initialState(props)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      this.props.page !== nextProps.page &&
      nextProps.page === constants.VIEW_BOGGLE
    ) {
      this.setState(this.initialState(nextProps))
    }
  }

  handleOnKeyDown(e) {
    const { inputString, found, score } = this.state
    const { board } = this.props

    if (e.keyCode === 13) {
      const lower = inputString.toLowerCase()
      if (found.has(lower) || !board.has(lower)) {
        this.setState({ inputString: '', error: true })
        return
      }

      const point = get(
        constants.SCORE_TABLE,
        `${lower.length}`,
        constants.SCORE_DEFAULT
      )
      this.setState({
        inputString: '',
        found: found.add(lower),
        error: false,
        score: score + point,
      })
    }
  }

  handleComplete() {
    const { found, score } = this.state
    const { dispatch } = this.props

    dispatch(action.saveScore(found, score))
  }

  render() {
    const { inputString, timerKey, found, error, score } = this.state
    const { currBoard, classes, dispatch, page } = this.props

    return (
      <Canvas visible={page === constants.VIEW_BOGGLE}>
        <Typography variant="headline">Boggle</Typography>
        <CountdownTimer
          boardKey={timerKey}
          onComplete={() => this.handleComplete()}
        />
        <div className={classes.flexDiv}>
          <div className={classes.autoFlex}>
            <BoggleBoard
              board={currBoard}
              key={currBoard}
              onComplete={() => this.setState({ timerKey: currBoard })}
            />
          </div>
          <div className={classes.autoFlex}>
            <Typography variant="headline">Score: {score}</Typography>
            <TextField
              label="Enter Word"
              style={{ margin: 8 }}
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              error={error}
              value={inputString}
              onChange={event =>
                this.setState({ inputString: event.target.value })
              }
              onKeyDown={e => this.handleOnKeyDown(e)}
            />
            <div className={classes.textFlex}>
              {[...found].map(val => (
                <div key={`div-${val}`} style={{ marginRight: 4 }}>
                  <SplitText
                    key={`typo-${val}`}
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    {val}
                  </SplitText>
                </div>
              ))}
            </div>
          </div>
        </div>
        <Button
          fullWidth
          variant="raised"
          color="primary"
          className={classes.primaryButton}
          onClick={() => this.handleComplete()}
        >
          Done
        </Button>
      </Canvas>
    )
  }
}

BoggleView = withStyles(styles)(
  connect(state => ({
    page: state.page,
    board: state.boards[state.currBoard],
    currBoard: state.currBoard,
  }))(BoggleView)
)
export default BoggleView
