import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'

import isEmpty from 'lodash/isEmpty'
import some from 'lodash/some'

import { Canvas, TabContainer } from '../component'

import styles from '../styles'
import * as action from '../redux/action'
import * as constants from '../constants'

class InputView extends React.Component {
  static displayName = 'views.InputView'

  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  initialState = props => ({
    inputType: isEmpty(props.boards)
      ? constants.INPUT_STRING
      : constants.INPUT_PAST,
    inputString: '',
    loading: false,
    error: false,
  })

  constructor(props) {
    super(props)
    this.state = this.initialState(props)
  }

  checkInvalid(val) {
    return val.length !== 1 || !val.match(/[a-z*]/i)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      this.props.page !== nextProps.page &&
      nextProps.page === constants.VIEW_INPUT
    ) {
      this.setState(this.initialState(nextProps))
    }
  }

  handleRequest(board) {
    const { inputString } = this.state
    const Http = new XMLHttpRequest()
    const url = `${constants.URL}/solve?board=${JSON.stringify(board)}`

    Http.open('GET', url)
    Http.onload = () => {
      switch (Http.status) {
        case 200:
          const cleanBoard = inputString.replace(/\s/g, '').toLowerCase()
          this.props.dispatch(
            action.saveBoard(cleanBoard, new Set(JSON.parse(Http.responseText)))
          )
          this.props.dispatch(
            action.pageChange(constants.VIEW_BOGGLE, cleanBoard)
          )
          break
        default:
          this.setState({ loading: false, error: true })
      }
    }

    Http.send()
  }

  handleBoggle() {
    const { inputString } = this.state

    // remove spaces and split
    const chars = inputString
      .replace(/\s/g, '')
      .toLowerCase()
      .split(',')
    // error checking
    if (chars.length !== 16 || some(chars, this.checkInvalid)) {
      this.setState({ error: true })
      return
    }

    this.setState({ loading: true, error: false })
    this.handleRequest(chars)
  }

  getPastItems() {
    const { boards } = this.props
    var boardList = [...boards]

    return boardList.map(val => (
      <MenuItem key={`menu-${val}`} value={val}>
        {val}
      </MenuItem>
    ))
  }

  handleOnKeyDown(e) {
    // handle enter
    if (e.keyCode === 13) {
      this.handleBoggle()
    }
  }

  render() {
    const { classes, dispatch, page, boards } = this.props
    const { inputType, inputString, error, loading } = this.state

    return (
      <Canvas visible={page === constants.VIEW_INPUT}>
        <Typography variant="headline">Input Board</Typography>
        <Tabs
          value={inputType}
          onChange={(event, val) => this.setState({ inputType: val })}
          indicatorColor="primary"
          textColor="primary"
          fullWidth
        >
          <Tab label="String" value={constants.INPUT_STRING} />
          <Tab
            label="Past"
            value={constants.INPUT_PAST}
            disabled={isEmpty(boards)}
          />
        </Tabs>
        <TabContainer visible={inputType === constants.INPUT_PAST}>
          <Select
            value={inputString.replace(/\s/g, '').toLowerCase()}
            onChange={event =>
              this.setState({ inputString: event.target.value })
            }
          >
            {this.getPastItems()}
          </Select>
        </TabContainer>
        <TabContainer visible={inputType === constants.INPUT_STRING}>
          <TextField
            label="Board String"
            style={{ margin: 8 }}
            placeholder="T, A, P, *, E, A, K, S, O, B, R, S, S, *, X, D"
            helperText="Insert * as wildcards!"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            disabled={loading}
            error={error}
            value={inputString}
            onChange={event =>
              this.setState({ inputString: event.target.value })
            }
            onKeyDown={e => this.handleOnKeyDown(e)}
          />
        </TabContainer>
        <Button
          variant="raised"
          color="primary"
          className={classes.primaryButton}
          onClick={() => this.handleBoggle()}
        >
          Boggle!
        </Button>
      </Canvas>
    )
  }
}

InputView = withStyles(styles)(
  connect(state => ({
    page: state.page,
    boards: Object.keys(state.boards),
  }))(InputView)
)
export default InputView
