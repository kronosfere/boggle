import produce from 'immer'

import * as actions from './action'
import * as constants from '../constants'

const initialState = {
  page: constants.VIEW_INPUT,
  currBoard: undefined,
  boards: {},
  score: 0,
  found: [],
}

export default function(state = initialState, action) {
  return produce(state, draft => {
    switch (action.type) {
      case actions.PAGE_CHANGE:
        draft.page = action.page
        if (action.currBoard) {
          draft.currBoard = action.currBoard
        }
        break
      case actions.SAVE_BOARD:
        draft.boards[action.board] = action.dict
        break
      case actions.SAVE_SCORE:
        draft.found = action.found
        draft.score = action.score
        draft.page = constants.VIEW_SCORE
      default:
    }
  })
}
