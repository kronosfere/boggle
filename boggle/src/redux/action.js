export const PAGE_CHANGE = 'PAGE_CHANGE'
export const pageChange = (page, currBoard = undefined) => ({
  type: PAGE_CHANGE,
  page: page,
  currBoard: currBoard,
})

export const SAVE_BOARD = 'SAVE_BOARD'
export const saveBoard = (board, dict) => ({
  type: SAVE_BOARD,
  board: board,
  dict: dict,
})

export const SAVE_SCORE = 'SAVE_SCORE'
export const saveScore = (found, score) => ({
  type: SAVE_SCORE,
  found: found,
  score: score,
})
