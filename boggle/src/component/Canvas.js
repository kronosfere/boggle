import React from 'react'
import PropTypes from 'prop-types'
import posed, { PoseGroup } from 'react-pose'

import Paper from '@material-ui/core/Paper'
import CssBaseline from '@material-ui/core/CssBaseline'
import withStyles from '@material-ui/core/styles/withStyles'

const styles = theme => ({
  layout: {
    width: 'auto',
    display: 'block',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      width: '80%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`,
  },
})

const PosedDiv = posed.div({
  enter: { y: 0, opacity: 1 },
  exit: { y: 50, opacity: 0 },
})

class Canvas extends React.Component {
  static displayName = 'component.Canvas'

  static propTypes = {
    classes: PropTypes.object.isRequired,
    visible: PropTypes.bool.isRequired,
  }

  render() {
    const { classes, children, visible } = this.props
    return (
      <PoseGroup>
        {!!visible && (
          <PosedDiv key="transition-wrapper">
            <CssBaseline />
            <main className={classes.layout}>
              <Paper className={classes.paper}>{children}</Paper>
            </main>
          </PosedDiv>
        )}
      </PoseGroup>
    )
  }
}

Canvas = withStyles(styles)(Canvas)
export default Canvas
