import React from 'react'
import PropTypes from 'prop-types'
import posed from 'react-pose'

import withStyles from '@material-ui/core/styles/withStyles'

const styles = theme => ({
  backing: {
    width: '80%',
    height: '10px',
    backgroundColor: '#aaaaaa',
    borderRadius: '10px',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '10px',
    marginBottom: '10px',
  },
  bar: {
    height: '100%',
    backgroundColor: '#757ce8',
    borderRadius: '10px',
    margin: '1px',
  },
})

const Bar = posed.div({
  end: {
    width: '0%',
    transition: { duration: 10 * 60 * 1000, ease: 'linear' },
  },
  start: { delay: 200, width: '100%', transition: { duration: 300 } },
})

class CountdownTimer extends React.Component {
  static displayName = 'component.CountdownTimer'

  static propTypes = {
    classes: PropTypes.object.isRequired,
    boardKey: PropTypes.string.isRequired,
    onComplete: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { startTime: false }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.props.boardKey !== nextProps.boardKey) {
      this.setState({ startTime: true })
    }
  }

  handleTimeout() {
    const { startTime } = this.state
    const { onComplete } = this.props

    if (startTime) {
      this.setState({ startTime: false })
      onComplete()
    }
  }

  render() {
    const { startTime } = this.state
    const { boardKey, classes, onComplete } = this.props

    return (
      <div className={classes.backing}>
        <Bar
          className={classes.bar}
          initialPose="end"
          pose={startTime ? 'end' : 'start'}
          onPoseComplete={() => this.handleTimeout()}
        />
      </div>
    )
  }
}

CountdownTimer = withStyles(styles)(CountdownTimer)
export default CountdownTimer
