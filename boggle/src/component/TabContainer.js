import React from 'react'
import PropTypes from 'prop-types'
import posed, { PoseGroup } from 'react-pose'

const PosedDiv = posed.div({
  enter: { x: 0, opacity: 1, transition: { duration: 200 } },
  exit: { x: -50, opacity: 0, transition: { duration: 200 } },
})

class TabContainer extends React.Component {
  static displayName = 'component.TabContainer'

  static propTypes = {
    visible: PropTypes.bool.isRequired,
  }

  render() {
    const { children, visible } = this.props
    return (
      <PoseGroup style={{ width: '100%' }}>
        {!!visible && <PosedDiv key="transition-wrapper">{children}</PosedDiv>}
      </PoseGroup>
    )
  }
}

export default TabContainer
