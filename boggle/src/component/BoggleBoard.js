import React from 'react'
import PropTypes from 'prop-types'
import posed from 'react-pose'

import delay from 'lodash/delay'

import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'

const styles = theme => ({
  board: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      width: '50%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  item: {
    width: '22%',
    height: 'calc(width)',
    borderRadius: '5px',
    padding: '2%',
    margin: '1%',
    backgroundColor: '#aaaaaa',
  },
})

const Board = posed.div({
  open: {
    y: 0,
    opacity: 1,
    delayChildren: 300,
    staggerChildren: 50,
  },
  closed: {
    y: 50,
    delay: 300,
  },
})

const Item = posed.div({
  open: { y: 0, opacity: 1 },
  closed: { y: 25, opacity: 0 },
})

class BoggleBoard extends React.Component {
  static displayName = 'component.BoggleBoard'

  static propTypes = {
    classes: PropTypes.object.isRequired,
    board: PropTypes.string.isRequired,
    onComplete: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { isOpen: false }
    delay(() => this.setState({ isOpen: true }), 100)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.props.board !== nextProps.board) {
      this.setState({ isOpen: false })
      delay(() => this.setState({ isOpen: true }), 100)
    }
  }

  render() {
    const { isOpen } = this.state
    const { classes, board, onComplete } = this.props

    const chars = board.split(',')

    return (
      <Board
        className={classes.board}
        key="BoggleBoard"
        pose={isOpen ? 'open' : 'closed'}
        onPoseComplete={onComplete}
      >
        {chars.map((val, idx) => (
          <Item className={classes.item} key={`BoggleBoard-${idx}`}>
            <Typography variant="headline" style={{ textAlign: 'center' }}>
              {val.toUpperCase()}
            </Typography>
          </Item>
        ))}
      </Board>
    )
  }
}

BoggleBoard = withStyles(styles)(BoggleBoard)
export default BoggleBoard
